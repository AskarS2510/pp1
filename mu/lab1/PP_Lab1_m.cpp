﻿#include "mpi.h"
#include <iostream>
#include <sstream>
#include <math.h>
int main(int argc, char* argv[])
{
	// ������������� ����������
	int MyID, NumProc;
	long long int N;
	double tstart, tfinish;
	// ������������� MPI
	int ierror = MPI_Init(&argc, &argv);
	if (ierror != MPI_SUCCESS)
		printf("MPI initialization error!");
	MPI_Comm_size(MPI_COMM_WORLD, &NumProc);
	MPI_Comm_rank(MPI_COMM_WORLD, &MyID);
	if (MyID == 0)
	{
		std::stringstream convert(argv[1]);
		if (!(convert >> N)) // ��������� �����������
			N = 0;
	}
	MPI_Barrier(MPI_COMM_WORLD);
	// ������ � ����������
	tstart = MPI_Wtime();
	MPI_Bcast(&N, 1, MPI_LONG_LONG_INT, 0, MPI_COMM_WORLD);
	double sum = 0.0;
	N++;
	long long int h = N / NumProc;
	long long int m = N % NumProc;
	switch (m)
	{
	case 0:
		if (MyID == 0)
		{
			for (long long int i = 1; i < h; i++)
			{
				sum += pow(-1, i) * log(i) / i;
			}
		}
		else
		{
			for (long long int i = MyID * h; i < (MyID + 1) * h; i++)
			{
				sum += pow(-1, i) * log(i) / i;
			}
		}

		break;
	default:
		if (MyID == 0)
		{
			h++;
			for (long long int i = 1; i < h; i++)
			{
				sum += pow(-1, i) * log(i) / i;
			}
		}
		else
		{
			if (MyID < m)
			{
				h++;
				for (long long int i = MyID * h; i < (MyID + 1) * h; i++)
				{
					sum += pow(-1, i) * log(i) / i;
				}
			}
			else
			{
				for (long long int i = MyID * h + m; i < (MyID + 1) * h + m; i++)
				{
					sum += pow(-1, i) * log(i) / i;
				}
			}
		}
		break;
	}
	double res;
	MPI_Reduce(&sum, &res, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
	MPI_Barrier(MPI_COMM_WORLD);
	tfinish = MPI_Wtime() - tstart;

	MPI_Finalize();

	if (MyID == 0)
	{
		N--;
		fprintf(stdout, "Time = %lf\n", tfinish);
		fprintf(stdout, "N = %lld\n", N);
		fprintf(stdout, "Sum = %lf\n", res);
	}
	return 0;
}
